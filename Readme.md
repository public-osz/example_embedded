
Requirements
```
- Process reads two voltages - v1 and v2
- if v1 or v2 is 1.5V or higher LED1 or LED2 should turn on, respectively
- if v1 or v2 is 1.5V or higher then 'Warning' should be printed on LCD
- if sum of v1 and v2 is higher than 5V then 'Alarm' should be printed on LCD
- if v1 or v2 is 3.3V or higher then 'Alarm' should be printed on LCD
- if v1 and v2 are without limits then 'Correct' should be printed on LCD
- Using average values or not should be easily configurable
```
